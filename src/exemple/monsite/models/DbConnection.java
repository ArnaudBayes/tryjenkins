package exemple.monsite.models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {

	private String chaineCnx="jdbc:oracle:thin:@localhost:1521:xe";
	private String login="hr";
	private String password="hr";
	private Connection  connection;
	private static DbConnection instance;
	
	
	private DbConnection() throws SQLException
	{
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver");
			this.connection = DriverManager.getConnection(chaineCnx, login, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("Erreur Driver Base de donn�es");
		}
	}
	
	public Connection getConnection()
	{
		return connection;
	}
	
	public static DbConnection getInstance() throws SQLException
	{
		if (instance == null)
		{
			instance =  new DbConnection();
		}
		else
		{
			if (instance.getConnection().isClosed())
			{
				instance = new DbConnection();
			}
		}
		return instance;
		
				
		
	}
		
	
}