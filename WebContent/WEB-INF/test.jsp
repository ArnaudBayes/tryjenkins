<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
 <body>
   <h1>Page test.jsp</h1>
   <%
     String ch = "<ul>";
     for(int i=1; i <= 5; i++)
     {
    	ch+="<li>Item " + i + "</li>";
     }
    ch+="</ul>";
    out.println(ch);
   %>
   <h2>Infos get</h2>
   
   <!-- Sciptlet pour fonctions "<!"  -->
   <%!
    String aff()
    {
	   return "<h3>Welcome Alexander</h3>";
    }
   %>
   
   <!-- Sciptlet de base  -->
   <%
    if (request.getAttribute("txtNom") != null && request.getAttribute("txtAge")!= null)
    {
    out.println(aff());	
    out.println("Nom: " + request.getAttribute("txtNom").toString().toUpperCase() );
    }
   %>
   
   <!-- Sciptlet de sortie de valeur -->
  <p>Age:  <%=request.getAttribute("txtAge").toString() %> ans</p>
	 
 </body>
</html>