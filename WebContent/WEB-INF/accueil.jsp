<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Html5</title>
<meta charset="utf-8"> 
<link rel="stylesheet" type="text/css" href="assets/css/gabaritSemantique.css">

</head>
<body>
      <div id="gnl">
       <%@include file="includes/header.jsp" %>
       
        <main>
         <%@include file="includes/aside.jsp" %>

          <section>
            <h1>Sujet de la page</h1>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, 
            adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
            <article>
              <h2>Article 1</h2>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, 
              adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
            </article>
            <article>
              <h2>Article 2</h2>
              <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet,
               adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam.Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</p>
            </article>
          </section>
           <%@include file="includes/archive.jsp" %>
        </main>
       <%@include file="includes/footer.jsp" %>
      </div>

</body>
</html>
