<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page import="java.sql.*" %>
<%@ page import="exemple.monsite.models.DbConnection" %>   
<aside>
  <nav>
    <ul>
      <li><a href="">Tous départements</a></li>
  
   
    <%
        Connection cnx= DbConnection.getInstance().getConnection();
	    Statement stm = cnx.createStatement();
		String strSQL = "SELECT department_id, department_name from departments order by department_name";
				
		ResultSet rsDept = stm.executeQuery(strSQL);
		
	    String ch = "";
		int i=1;
		while(rsDept.next() == true)
		{
	     ch+="<li><a href='accueil?idDept=" + rsDept.getString("department_id")+"'>"+  rsDept.getString("department_name") + "</a></li>"; 
		}
	    //ch+="</ul>";
	    out.println(ch);
		rsDept.close();
		stm.close();    
    %>
     </ul>
  </nav>
</aside>